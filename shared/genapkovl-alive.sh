#!/bin/sh -e

HOSTNAME="$1"
if [ -z "$HOSTNAME" ]; then
	echo "usage: $0 hostname"
	exit 1
fi

DIR_CRNT="$( pwd )"
DIR_VGETTY="/mnt/vgetty"

cleanup() {
	rm -rf "$tmp"
	cd "$DIR_VGETTY"
	make clean
}

makefile() {
	OWNER="$1"
	PERMS="$2"
	FILENAME="$3"
	cat > "$FILENAME"
	chown "$OWNER" "$FILENAME"
	chmod "$PERMS" "$FILENAME"
}

rc_add() {
	mkdir -p "$tmp"/etc/runlevels/"$2"
	ln -sf /etc/init.d/"$1" "$tmp"/etc/runlevels/"$2"/"$1"
}

tmp="$(mktemp -d)"
trap cleanup EXIT

cd "$DIR_VGETTY"
make clean
make
cp "$DIR_VGETTY/vgetty" "/mnt/overlay/sbin"
cd "$DIR_CRNT"

mkdir -p "$tmp"/etc
makefile root:root 0644 "$tmp"/etc/hostname <<EOF
$HOSTNAME
EOF

rsync -av "/mnt/overlay/" "$tmp"
rsync -av "/mnt/overlay_secret/" "$tmp"
chown -R root:root "$tmp"/root
chown -R root:root "$tmp"/etc/ssh
chown -R root:root "$tmp"/etc/network
chown root:root "$tmp"/sbin/vgetty
chmod 755 "$tmp"/sbin/vgetty

mkdir -p "$tmp"/etc/apk
makefile root:root 0644 "$tmp"/etc/apk/world <<EOF
alpine-base
tmux
ddrescue
xorg-server
xf86-video-vesa
i3wm
st
xf86-input-libinput
xf86-input-evdev
eudev
openssh
rsync
dmenu
ttf-inconsolata
EOF

# TODO: dynamic version
makefile root:root 0644 "$tmp"/etc/apk/repositories <<EOF
http://mirror1.hs-esslingen.de/pub/Mirrors/alpine/edge/main
http://mirror1.hs-esslingen.de/pub/Mirrors/alpine/edge/community
http://mirror1.hs-esslingen.de/pub/Mirrors/alpine/edge/testing
EOF

rc_add devfs sysinit
rc_add dmesg sysinit

rc_add mdev sysinit
rc_add hwdrivers sysinit
rc_add udev sysinit
rc_add udev-trigger sysinit
#rc_add udev-postmount default

rc_add modloop sysinit

rc_add hwclock boot
rc_add modules boot
rc_add sysctl boot
rc_add hostname boot
rc_add bootmisc boot
rc_add syslog boot
rc_add networking boot

rc_add sshd default

rc_add mount-ro shutdown
rc_add killprocs shutdown
rc_add savecache shutdown

#tar -c -C "$tmp" etc | gzip -9n > $HOSTNAME.apkovl.tar.gz

cd "$tmp"; tar -c * | gzip -9n > "$DIR_CRNT"/$HOSTNAME.apkovl.tar.gz
