#!/bin/sh

export SHELL="/bin/sh"
export F_STATUS="/mnt/status"

. /mnt/tools.sh

cd

[ -d "$HOME/.abuild" ] || abuild-keygen -ianq

if [ ! -d "$HOME/aports" ] ; then
    echo "git clone aports"
    git clone -q git://git.alpinelinux.org/aports
fi

[ -f "$HOME/aports/scripts/genapkovl-alive.sh" ] || ln -s /mnt/genapkovl-alive.sh "$HOME/aports/scripts"
[ -f "$HOME/aports/scripts/mkimg.alive.sh" ] || ln -s /mnt/mkimg.alive.sh "$HOME/aports/scripts"

cd "$HOME/aports/scripts"
mkdir -p "$HOME/iso"
sudo rm -rf $HOME/tmp/apkroot-* $HOME/iso/*
chk_status clearworkdir && sudo rm -rf $HOME/tmp/*

sh mkimage.sh --tag edge \
    --outdir "$HOME/iso" \
    --workdir "$HOME/tmp" \
    --arch x86_64 \
    --repository http://dl-cdn.alpinelinux.org/alpine/edge/main \
    --extra-repository  http://dl-cdn.alpinelinux.org/alpine/edge/community \
    --extra-repository  http://dl-cdn.alpinelinux.org/alpine/edge/testing \
    --profile alive
