[ -f ./cfg.sh ] && . cfg.sh

mnt() {
    mountpoint -q "$DIR_CHROOT/proc" || sudo mount -t proc proc "$DIR_CHROOT/proc"

    if ! mountpoint -q "$DIR_CHROOT/sys"; then
        sudo mount --rbind /sys "$DIR_CHROOT/sys"
        sudo mount --make-rslave "$DIR_CHROOT/sys"
    fi

    if ! mountpoint -q "$DIR_CHROOT/dev"; then
        sudo mount --rbind /dev "$DIR_CHROOT/dev"
        sudo mount --make-rslave "$DIR_CHROOT/dev"
    fi

    mountpoint -q "$DIR_CHROOT/mnt" || sudo mount --rbind "$DIR_SHARED" "$DIR_CHROOT/mnt"
}

umnt() {
    mountpoint -q "$DIR_CHROOT/dev" && sudo umount -l "$DIR_CHROOT/dev"{/shm,/pts,}
    mountpoint -q "$DIR_CHROOT/sys" && sudo umount -R "$DIR_CHROOT/sys"
    mountpoint -q "$DIR_CHROOT/proc" && sudo umount -R "$DIR_CHROOT/proc"
    mountpoint -q "$DIR_CHROOT/mnt" && sudo umount -R "$DIR_CHROOT/mnt"
}

chk_status() {
    [ -f "$F_STATUS" ] || touch "$F_STATUS"
    grep -q "^$1\$" "$F_STATUS"
}

add_status() {
    [ -f "$F_STATUS" ] || touch "$F_STATUS"
    grep -q "^$1\$" "$F_STATUS" && return
    echo "$1" >> "$F_STATUS"
}
