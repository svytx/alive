profile_alive() {
    profile_base
    hostname=alive
    image_ext="iso"
    arch="x86_64"
    output_format="iso"
    #kernel_cmdline="nomodeset"
    kernel_addons="xtables-addons"
    apks="$apks tmux ddrescue xorg-server xf86-video-vesa i3wm st xf86-input-libinput eudev rsync dmenu ttf-inconsolata"
    apkovl="genapkovl-alive.sh"
}
