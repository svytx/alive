/* See COPYING file for copyright and license details. */

/* automatic login with this user */
static const char *usr = "root";

/* login program invoked */
static const char *loginprog = "/bin/login";

/* Do not call vhangup() on the tty. */
static const int nohangup = 0;

/* sleep time before exiting */
#define SLEEP_TIME 5
