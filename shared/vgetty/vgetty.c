/*
 *  Copyright (C) 1996 Florian La Roche  <laroche@redhat.com>
 *  Copyright (C) 2002, 2003 Red Hat, Inc
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <stdarg.h>

#include "conf.h"

static void err(const char *fmt, ...) {
    va_list va_alist;

    va_start(va_alist, fmt);
    vprintf(fmt, va_alist);
    va_end(va_alist);
    sleep(SLEEP_TIME);
    exit(EXIT_FAILURE);
}

static void usg(void) {
    err("usage: vgetty tty (with e.g. tty=/dev/tty1)\n");
}

// set up tty as standard { input, output, error }
int main(int argc, char **argv) {
    if (argc != 2) usg();

    char *tty = argv[1];
    struct sigaction sa, sa_old;
    int fd;

    putenv("TERM=linux");

    // Set up new standard input.

    /* There is always a race between this reset and the call to
       vhangup() that s.o. can use to get access to your tty. */
    if (chown(tty, 0, 0) || chmod(tty, 0600))
        if (errno != EROFS) err("%s: %s\n", tty, strerror(errno));

    sa.sa_handler = SIG_IGN;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGHUP, &sa, &sa_old);

    /* vhangup() will replace all open file descriptors in the kernel
       that point to our controlling tty by a dummy that will deny
       further reading/writing to our device. It will also reset the
       tty to sane defaults, so we don't have to modify the tty device
       for sane settings. We also get a SIGHUP/SIGCONT.
     */
    if ((fd = open(tty, O_RDWR, 0)) < 0)
        err("%s: cannot open tty: %s\n", tty, strerror(errno));

    if (ioctl (fd, TIOCSCTTY, (void *) 1) == -1)
        err("%s: no controlling tty: %s\n", tty, strerror(errno));

    if (!isatty(fd))
        err("%s: not a tty\n", tty);

    if (nohangup == 0) {
        if (vhangup())
            err("%s: vhangup() failed", tty);
        /* Get rid of the present stdout/stderr. */
        close(2);
        close(1);
        close(0);
        close(fd);
        if ((fd = open(tty, O_RDWR, 0)) != 0)
            err("%s: cannot open tty: %s\n", tty, strerror(errno));
        if (ioctl(fd, TIOCSCTTY, (void *) 1) == -1)
            err("%s: no controlling tty: %s\n", tty, strerror(errno));
    }
    /* Set up stdin/stdout/stderr. */
    if (dup2(fd, 0) != 0 || dup2(fd, 1) != 1 || dup2(fd, 2) != 2)
        err("%s: dup2(): %s\n", tty, strerror(errno));
    if (fd > 2)
        close(fd);

    sigaction(SIGHUP, &sa_old, NULL);

    fflush(stdout);

    execl(loginprog, loginprog, "-f", usr, NULL);
    err("%s: can't exec %s: %s\n", tty, loginprog, strerror(errno));
    sleep(SLEEP_TIME);
    exit(EXIT_FAILURE);
}
