#!/bin/bash

. ./shared/tools.sh

DEV=$1
PART=${DEV}1
SRC="$DIR_TMP/mnt/iso"
DST="$DIR_TMP/mnt/usb"

mkdir -p "$DIR_TMP/mnt/"{iso,usb}

sudo mount "$DIR_CHROOT/home/build/iso/"*.iso "$SRC"
sudo mount "$PART" "$DST"
sudo cp -v "$SRC/alive.apkovl.tar.gz" "$DST"
sudo rm -rf "$DST/alive"
sudo mkdir -pv "$DST/alive"
sudo cp -v "$SRC/.alpine-release" "$DST/alive"
sudo cp -v "$SRC/apks/.boot_repository" "$DST/alive"
sudo rsync -rv "$SRC/apks/x86_64" "$DST/alive"
for f in "$SRC/boot/"{config,initramfs,modloop,System.map,vmlinuz}-*; do
    sudo cp -v $f "$DST/alive/$(basename ${f%%-*})"
done

sudo umount "$DIR_TMP/mnt/"{iso,usb}
