# ALive - alpine linux based rescue system

## Building
+ Edit cfg.sh
+ run bld.sh

### bld.sh
+ creates chroot: bld_chroot.sh
+ prepares _build_ user
+ builds image inside chroot: _shared/bld.sh_

### shared/status: controls build
+ chroot        skip chroot creation
+ buildtools    skip installing required Alpine Linux packages
+ clearworkdir  clear workdir before creating an image

### Cleanup
cleanup.sh - start fresh

## Installing to usb
Run usb.sh _/dev/usbdevice_.

Assumes:
+ x86_64
+ drive is MBR formatted
+ flash drive is not mounted
+ FAT partitioned
+ syslinux is installed
+ syslinux/syslinux.cfg exists in drive's root directory and is configured
+ uses first partition

### syslinux.cfg snippet
```
KERNEL /alpine/vmlinuz-vanilla
INITRD /alpine/initramfs-vanilla
DEVICETREEDIR /boot/dtbs
APPEND modules=loop,squashfs,sd-mod,usb-storage modloop=/alpine/modloop-vanilla quiet
```

### Notes
To build an image from scratch, remove $DIR_CHROOT/home/build/tmp/*

Private overlay files go to $DIR_SHARED/overlay_secret (e.g. ssh keys)
