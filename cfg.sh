export ALPINE_VER=3.8.2
export ALPINE_MIRROR="http://dl-cdn.alpinelinux.org/alpine/"
export ARCH=x86_64

# alpine 'abuild' goup id
export ALPINE_GID_ABUILD=300

# alpine 'build' user id
export ALPINE_UID_BUILD=1000

export DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export DIR_TMP="$DIR/tmp"
export DIR_CHROOT="$DIR/chroot"
export DIR_SHARED="$DIR/shared"
export F_STATUS="$DIR_SHARED/status"
