#!/bin/bash

echo bld_chroot

TAR="alpine-minirootfs-$ALPINE_VER-$ARCH.tar.gz"

if [ -d "$DIR_CHROOT" ]; then
    if [ "$(ls -A $DIR_CHROOT)" ]; then
        echo "Err: $DIR_CHROOT is not empty. Quiting."
        exit 1
    fi
else
    mkdir -p "$DIR_CHROOT"
fi

[ -d "$DIR_TMP" ] || mkdir -p "$DIR_TMP"

if [ ! -f "$DIR_TMP/$TAR" ]; then
    wget -qc -P "$DIR_TMP" "${ALPINE_MIRROR}v${ALPINE_VER%.*}/releases/$ARCH/$TAR"
fi

sudo tar xf "$DIR_TMP/$TAR" -C "$DIR_CHROOT"
sudo chmod 755 "$DIR_CHROOT"
echo -e "nameserver 8.8.8.8\nnameserver 8.8.4.4" \
    | sudo tee "$DIR_CHROOT/etc/resolv.conf" > /dev/null
