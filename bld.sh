#!/bin/bash

. ./shared/tools.sh

if ! chk_status chroot ; then
    . cleanup.sh
    . bld_chroot.sh
    add_status chroot
fi

# add user build
if ! grep -q "^build:" "$DIR_CHROOT/etc/passwd"; then
    echo "add user build"
    echo -e "build:x:$ALPINE_UID_BUILD:$ALPINE_GID_ABUILD:build:/home/build:/bin/sh" \
        | sudo tee -a "$DIR_CHROOT/etc/passwd" > /dev/null
    sudo mkdir -p "$DIR_CHROOT/home/build"
    sudo chown $ALPINE_UID_BUILD:$ALPINE_GID_ABUILD "$DIR_CHROOT/home/build"
fi

# add user build to sudoers and require no password for sudo
if [ ! -d "$DIR_CHROOT/etc/sudoers.d" ]; then
    echo "add user build to sudoers"
    sudo mkdir -p "$DIR_CHROOT/etc/sudoers.d"
    sudo chmod 750 "$DIR_CHROOT/etc/sudoers.d"
    echo -e "build ALL=(ALL) NOPASSWD: ALL" \
        | sudo tee "$DIR_CHROOT/etc/sudoers.d/build" > /dev/null
    sudo chmod 440 "$DIR_CHROOT/etc/sudoers.d/build"
fi

sudo chown $ALPINE_UID_BUILD:$ALPINE_GID_ABUILD "$DIR_SHARED/bld.sh"

# Entering chroot

mnt

# Install required build tools
if ! chk_status buildtools ; then
    echo "installing build tools"
    sudo chroot "$DIR_CHROOT" /bin/sh -c '. /etc/profile && apk -q update && apk -q --no-cache add alpine-sdk build-base apk-tools alpine-conf busybox fakeroot syslinux xorriso grub-bios grub-efi squashfs-tools rsync'
    add_status buildtools
fi

sudo chroot "$DIR_CHROOT" /bin/sh -c '. /etc/profile && su build /mnt/bld.sh'

#umnt
